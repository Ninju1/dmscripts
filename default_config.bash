declare -A colpick_list
colpick_list[black]="#282a36"
colpick_list[red]="#ff5555"
colpick_list[green]="#5af78e"
colpick_list[yellow]="#f1fa8c"
colpick_list[blue]="#57c7ff"
colpick_list[magenta]="#ff6ac1"
colpick_list[cyan]="#8be9fd"
colpick_list[white]="#f1f1f0"
colpick_list[orange]="#ffb86c"
colpick_list[purple]="#bd9cf9"

# Search engine config
declare -A websearch
# Syntax:
# websearch[name]="https://www.example.com/search?q="

# Search Engines
websearch[bing]="https://www.bing.com/search?q="
websearch[brave]="https://search.brave.com/search?q="
websearch[duckduckgo]="https://duckduckgo.com/?q="
websearch[gemini search \(https\)]="https://portal.mozz.us/gemini/geminispace.info/search%3F"
websearch[google]="https://www.google.com/search?q="
websearch[qwant]="https://www.qwant.com/?q="
websearch[swisscows]="https://swisscows.com/web?query="
websearch[yandex]="https://yandex.com/search/?text="
# Information/News
websearch[bbcnews]="https://www.bbc.co.uk/search?q="
websearch[cnn]="https://www.cnn.com/search?q="
websearch[googlenews]="https://news.google.com/search?q="
websearch[wikipedia]="https://en.wikipedia.org/w/index.php?search="
websearch[wiktionary]="https://en.wiktionary.org/w/index.php?search="
# Social Media
websearch[reddit]="https://www.reddit.com/search/?q="
websearch[odysee]="https://odysee.com/$/search?q="
websearch[youtube]="https://www.youtube.com/results?search_query="
# Online Shopping
websearch[amazon]="https://www.amazon.com/s?k="
websearch[craigslist]="https://www.craigslist.org/search/sss?query="
websearch[ebay]="https://www.ebay.com/sch/i.html?&_nkw="
websearch[gumtree]="https://www.gumtree.com/search?search_category=all&q="
# Linux
websearch[archaur]="https://aur.archlinux.org/packages/?O=0&K="
websearch[archpkg]="https://archlinux.org/packages/?sort=&q="
websearch[archwiki]="https://wiki.archlinux.org/index.php?search="
websearch[debianpkg]="https://packages.debian.org/search?suite=default&section=all&arch=any&searchon=names&keywords="
# Development
websearch[github]="https://github.com/search?q="
websearch[gitlab]="https://gitlab.com/search?search="
websearch[googleOpenSource]="https://opensource.google/projects/search?q="
websearch[sourceforge]="https://sourceforge.net/directory/?q="
websearch[stackoverflow]="https://stackoverflow.com/search?q="
