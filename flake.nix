{
  description = "A collection of dmenu scripts that @dwt1 has found useful in their day-to-day activities as a desktop Linux user.";

  inputs.nixpkgs.url = "github:NixOS/nixpkgs/nixos-22.11";

  outputs = { self, nixpkgs }:
    let
      pkgs = import nixpkgs { system = "x86_64-linux"; };
      dmscripts = pkgs.callPackage ./default.nix {};
    in
    {

      packages.x86_64-linux.dm-translate = dmscripts.dm-translate;

      packages.x86_64-linux.dm-wifi = dmscripts.dm-wifi;

      packages.x86_64-linux.dm-websearch = dmscripts.dm-websearch;

      packages.x86_64-linux.dm-kill = dmscripts.dm-kill;

      packages.x86_64-linux.dm-ip = dmscripts.dm-ip;

      packages.x86_64-linux.dm-colpick = dmscripts.dm-colpick;

      packages.x86_64-linux.dm-man = dmscripts.dm-man;

      packages.x86_64-linux.dm-hello-world = pkgs.writeShellScriptBin "dm-hello-world" ''
      echo Hello world, from dmscripts!
    '';

      packages.x86_64-linux.default = self.packages.x86_64-linux.dm-hello-world;
    };
}
