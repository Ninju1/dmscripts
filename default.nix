{
  pkgs, lib ? pkgs.lib, stdenv ? pkgs.stdenv

, # launcherArgs - arguments passed to the launcher function
  launcherArgs ? "-i -l 10 -p"

, # launcher - e.g. ${pkgs.dmenu}/bin/dmenu for dmenu
  launcher ? "${pkgs.dmenu}/bin/dmenu"

, # term - the terminal, e.g. to open man pages
  # Example: "${pkgs.st}/bash/st"
  term ? "${pkgs.kitty}/bin/kitty"

, # configFile
  #   A dmscripts style config file containing all config for all scripts
  configFile ? ./default_config.bash

, # Helper script (e.g. bash functions to make writing dm scripts easier)
  helperScript ? ./scripts/_dm-helper.sh

, # Web browser command for scripts that want to open URLs
  webBrowserCmd ? "${pkgs.firefox}/bin/firefox"

  , ... }:
let
  inherit (lib) makeExtensible callPackageWith;

  defaultPkgArgs = {
    inherit
      launcher
      launcherArgs
      term
      configFile
      helperScript
      cp2cb
      webBrowserCmd;

    inherit (stdenv) mkDerivation;
    inherit (lib) makeBinPath;
  };

  callPackage = callPackageWith (pkgs // defaultPkgArgs);

  mkDmScript = callPackage ./nix/lib/mkDmScript.nix {};
  cp2cb = callPackage ./nix/pkgs/cp2cb.nix {};

  mkOverridableDmScript = filepath: args: import filepath ({ mkDmScript = callPackage mkDmScript; } // args);
in
makeExtensible (self: {
  dm-translate = mkOverridableDmScript ./nix/dm-translate.nix { inherit cp2cb; inherit (pkgs) jq libnotify curl coreutils; };
  dm-ip = mkOverridableDmScript ./nix/dm-ip.nix { inherit (pkgs) dnsutils jq iproute2 libnotify; inherit cp2cb; };
  dm-kill = mkOverridableDmScript ./nix/dm-kill.nix { inherit (pkgs) gawk ps; };
  dm-man = mkOverridableDmScript ./nix/dm-man.nix {};
  dm-colpick = mkOverridableDmScript ./nix/dm-colpick.nix { inherit cp2cb; inherit (pkgs) coreutils; };
  dm-websearch = mkOverridableDmScript ./nix/dm-websearch.nix { inherit (pkgs) jq coreutils; };
  dm-wifi = mkOverridableDmScript ./nix/dm-wifi.nix { inherit (pkgs) networkmanager coreutils gnused; };
})
