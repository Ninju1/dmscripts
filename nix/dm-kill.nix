{ mkDmScript,
  gawk,
  ps,
}:
mkDmScript {
  name = "dm-kill";
  scriptPath = ../scripts/dm-kill;
  binPackages = [
    gawk
    ps
  ];
}
