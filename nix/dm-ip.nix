{
  mkDmScript,
  dnsutils,
  jq,
  iproute2,
  cp2cb,
  libnotify
}:
mkDmScript {
  name = "dm-ip";
  scriptPath = ../scripts/dm-ip;
  binPackages = [
    dnsutils
    jq
    iproute2
    cp2cb
    libnotify
  ];
}
