{
  mkDmScript,

  jq,
  cp2cb,
  libnotify,
  curl,
  coreutils
}:
mkDmScript {
  name = "dm-translate";
  scriptPath = ../scripts/dm-translate;

  binPackages = [
    jq
    cp2cb
    libnotify
    curl
    coreutils
  ];
}
