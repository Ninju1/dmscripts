{
  mkDmScript,
  coreutils,
  jq
}:
mkDmScript {
  name = "dm-websearch";
  scriptPath = ../scripts/dm-websearch;
  binPackages = [ coreutils jq ];
}
