{
  mkDmScript,
  networkmanager,
  coreutils,
  gnused
}:
mkDmScript {
  name = "dm-wifi";
  scriptPath = ../scripts/dm-wifi;
  binPackages = [
    networkmanager
    coreutils
    gnused
  ];
}
