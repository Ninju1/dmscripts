{ mkDmScript,
  cp2cb,
  coreutils
}:
mkDmScript {
  name = "dm-colpick";
  scriptPath = ../scripts/dm-colpick;
  binPackages = [ cp2cb coreutils ];
}
