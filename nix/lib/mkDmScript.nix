{
  mkDerivation, makeWrapper, makeBinPath
}:
{
  name,
  binPackages ? [],
  scriptPath,

  term,
  launcher,
  launcherArgs,
  helperScript,
  webBrowserCmd,
  configFile
}:
mkDerivation {
  name = name;

  nativeBuildInputs = [ makeWrapper ];

  phases = [ "installPhase" "fixupPhase" ];

  installPhase = ''
    mkdir -p $out/bin
    cp ${scriptPath} $out/bin/${name}

    wrapProgram $out/bin/${name} \
      --set DMENU "${launcher} ${launcherArgs}" \
      --set DMTERM "${term}" \
      --set HELPER_SCRIPT "${helperScript}" \
      --set DMBROWSER "${webBrowserCmd}" \
      --set DMSCRIPTS_CONFIGS "${configFile}" \
      --set PATH "${makeBinPath binPackages}"
  '';
}
